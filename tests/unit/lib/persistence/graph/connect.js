/* eslint-disable */
jest.mock('neo4j-driver');
import 'babel-polyfill';
import { v1 as neo4j } from 'neo4j-driver';
import * as connect from '../../../../../src/lib/persistence/graph/connect';

describe('test graph connection functions', () => {

  test('connectAsync function should works as expected', async () => {
    const conn1 = await connect.connectAsync('bolt://user:pass@localhost:9000');
    expect(conn1).toEqual('bolt://localhost:9000');

    const conn2 = connect.connectAsync('bolt://user@localhost');
    conn2.then(uri => {
      expect(uri).toEqual('bolt://localhost:7687');
    });

    neo4j.__state.__driverFnShouldThrowException = true;

    try {
      const conn3 = await connect.connectAsync('http://neo4j');
    } catch (e) {
      expect(e.message).toEqual('Error connecting to fake neo4j database')
    }

    neo4j.__state.__driverFnShouldThrowException = false;
  })

  test('connect function should works as expected', () => {
    const conn1 = connect.connect('bolt://user:pass@localhost');
    expect(conn1).toEqual('bolt://localhost:7687');

    const conn2 = connect.connect('192.168.201.105');
    expect(conn2).toEqual('bolt://192.168.201.105:7687');
  })

})
