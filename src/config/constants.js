export default {

  // Env config
  ENV_PRODUCTION: 'production',
  ENV_DEVELOPMENT: 'development',
  ENV_STAGING: 'staging',
  ENV_TEST: 'test',

  // Application config
  CONFIG_APP_NAME: 'appName',
  CONFIG_DEBUG: 'debug',
  CONFIG_ENV: 'env',
  CONFIG_IP: 'ip',
  CONFIG_HTTPS: 'https',
  CONFIG_PORT: 'port',
  CONFIG_SECRET: 'secret',
  CONFIG_API_VERSION: 'apiVersion',
  CONFIG_LOGGER_FORMAT: 'loggerFormat',
  CONFIG_TOKEN_HEADER: 'tokenHeader',
  CONFIG_SENTRY_URL: 'sentryUrl',

  // MySQL config
  CONFIG_MYSQL_URL: 'mysqlUrl',
  CONFIG_MYSQL_OPTIONS: 'mysqlOptions',

  // MongoDB config
  CONFIG_MONGODB_URL: 'mongodbUrl',
  CONFIG_MONGODB_OPTIONS: 'mongodbOptions',

  // Neo4j config
  CONFIG_NEO4J_URL: 'neo4jUrl',
  CONFIG_NEO4J_OPTIONS: 'neo4jOptions',

  // Redis config
  CONFIG_REDIS_URL: 'redisUrl',

  // Authentication & Authorization
  TOKEN_HEADER: 'X-Authentication-Token',

  // Module events
  MODULE_ON_INIT: 'moduleInit',
  MODULE_ON_PRE_EXPOSE: 'modulePreExpose',
  MODULE_ON_EXPOSE: 'moduleExpose',
  MODULE_ON_POST_EXPOSE: 'modulePostExpose',
  MODULE_ON_LOAD: 'moduleLoaded',
};
