import { v4 } from 'uuid';
import { isString } from '../../utils';
import { MatchBuilder, Match, Node, Relation } from './query/builder';

export class Cypher {
  /**
   * @todo validate whether `neo4j` param is a valid connection instance
   */
  constructor(neo4j) {
    this.neo4j = neo4j;
  }

  /**
   * Run cypher query
   * @param {string}
   * @param {Object}
   * @returns {neo4j.result}
   */
  query(statement, parameters = {}) {
    const session = this.neo4j.session();
    const result = session.run(statement, parameters);
    result.subscribe({
      onCompleted: () => {
        session.close();
      },
    });
    return result;
  }
}

/**
 * Convert object or string to neo4j properties/attributes notation
 * @param {Object|string} obj
 * @returns {string}
 */
export function objectToStringProps(obj) {
  if (isString(obj)) {
    obj = JSON.parse(obj);
  }

  if (Object.keys(obj).length) {
    const props = [];
    Object.keys(obj).forEach((k) => {
      const value = isString(obj[k]) ? `'${obj[k]}'` : obj[k];
      props.push(`${k}: ${value}`);
    });

    return props.join(', ');
  }

  return '';
}

/**
 * Node label to array of labels
 * It simply convert 'Label1:Label2:Label3' to [Label1, Label2, Label3]
 * @param {string} label
 * @returns {array}
 */
export function labelToArray(label) {
  if (Array.isArray(label)) {
    return label;
  }

  if (isString(label) && label.indexOf(':')) {
    return label.split(':');
  }

  return [];
}

/**
 * Generate new UUID v4
 * @returns {string}
 */
export function uuid() {
  return v4();
}

/**
 * Create new Node
 * @param {string} alias
 * @param {array|string} labels
 * @param {Object} properties
 * @return {Node}
 */
export function n(alias, labels = [], properties = {}) {
  return new Node(alias, labels, properties);
}

/**
 * Create new Relation
 * @param {string} alias
 * @param {array|string} labels
 * @param {Object} properties
 * @param {string} direction
 * @param {string} range
 * @return {Node}
 */
export function r(alias, labels = [], properties = {}, direction = 'omni', range) {
  return new Relation(alias, labels, properties, direction, range);
}

/**
 * Create new MatchBuilder
 * @param {Match|Node} node
 * @return {MatchBuilder}
 */
export function match(node) {
  const builder = new MatchBuilder();

  if (node instanceof Match) {
    return builder.add(node);
  } else if (node instanceof Node) {
    const m = new Match();
    m.add(node);
    return builder.add(m);
  }

  return builder;
}
