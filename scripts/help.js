const chalk = require('chalk');
const def = require('../src/config/definitions');
const task = require('./task');

const commands = ['config', 'db', 'deploy'];
const command = process.argv[2];

module.exports = task('help', () => {
  if (!commands.includes(command)) {
    throw new Error(`Unknown command: ${command}`);
  }

  switch (command) {
    case 'config':
      console.log(chalk.bgBlue('Available config:'));
      Object.keys(def.default).forEach((k) => {
        console.log(`${chalk.cyan(k)}${Array(20 - k.length).join(' ')}: ${def.default[k].doc}`);
      });
      break;
    case 'db':
      console.error('Not yet implemented.');
      break;
    case 'deploy':
      console.error('Not yet implemented.');
      break;
    default:
      console.error('Not yet implemented.');
      break;
  }
});
