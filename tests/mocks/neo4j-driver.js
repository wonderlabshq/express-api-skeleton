/* eslint-disable */
const __state = {
  __driverFnShouldThrowException: false,
};

const driver = uri => {
  if (__state.__driverFnShouldThrowException) {
    throw new Error('Error connecting to fake neo4j database');
  }

  return uri;
};

module.exports = {
  v1: {
    driver,
    auth: {
      basic: () => {}
    },
    __state
  },
};
