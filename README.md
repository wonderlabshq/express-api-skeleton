# Express API Skeleton

### Requirements
- NodeJS version 7.x or higher
- MySQL server
- MongoDB server
- Neo4J version 2 or higher
- Redis server in production

### Documentation

#### Directory Structure
Each module will be placed under `src/modules` directory. As a rule of thumb, break modules into small chunks. All public API should be exposed via `index.js`, even there is no limitation in NodeJS to access public API via `index.js`, it is considered best practice as it will prevent us to import something unknown and prevent tightly-coupled modules. Think the `index.js` as a gateway to exchange things.

Example:

```js
// src/modules/users/model.js
class Person {
  static id: Int;
  static name: String;
}
```

```js
// src/modules/users/index.js
import model from './model';
export model;
```

```js
// src/modules/comments/model.js
import { model } from '../users';

class Comment {
  static id: Int;
  static person: model.Person;
}
```

#### Configuration
To see all possible config, run `yarn run help:config`. You can load multiple config files by setting `CONFIG_FILES` env var. All the config files will be merged and the last config file will override the previous one. This is usefull if you have different dataset to test out.

Example:

```bash
CONFIG_FILES=config/local.json,config/dev.json yarn run start
```

#### Working with Database
- Always use uuid v4 to create node ID

#### CLI Commands
Here's list of commands you can use:
- `yarn run help:config` to see all possible config
- `yarn run lint` run linter (eslint with airbnb config)
- `yarn run check` run flow static typing check. https://github.com/facebook/flow
- `yarn run test` run jest test
- `yarn run test:coverage` run jest test with code coverage
- `yarn run build` transpile all js files under `src` directory into `build` folder
- `yarn run build:watch` build with watcher enabled
- `yarn run db:version` shows current db schema version
- `yarn run db:migrate` run migration script
- `yarn run db:migrate:rollback` rollback to previous db schema version
- `yarn run db:migration` create new migration file
- `yarn run db:seed` run seeder script
- `yarn run deploy` build a docker image and deploy the image via ssh
- `yarn run start` run app server

### Contributing
We will be using gitflow as a standard collaborating flow. This is a good start to read https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow.

Basically there will be 4 main branches:
- master branch, the stable code
- develop branch, where all feature branches will be merged to
- feature branches, this will be your main working branch when doing a feature
- hotfix braches, hopefully we won't create one

Before you commit and push to your branch, make sure you have tested it out, and rebase your branch if necessary. After you've finished with your feature branch, don't forget to create a pull request (PR) to develop branch. Keep the PR small and clean!

Happy contributing!
