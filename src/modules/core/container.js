import Promise from 'bluebird';
import raven from 'raven';
import { get } from '../../config';
import c from '../../config/constants';
import Container from '../../lib/container';
import { connect as db } from '../../lib/persistence/db/connect';
import { connect as graph } from '../../lib/persistence/graph/connect';
import { connectAsync as mongo } from '../../lib/persistence/mongo/connect';

const def = {
  factory: {},
  prototype: {},
};

/**
 * Create sentry client
 * Sentry client will only be created on staging or production environment
 */
def.factory.sentry = () => {
  if (get(c.CONFIG_ENV) in [c.ENV_PRODUCTION, c.ENV_STAGING]) {
    return raven.Client(get(c.CONFIG_SENTRY_URL));
  }

  return null;
};

/**
 * Create knex connection to mysql database
 */
def.factory.db = () => db(get(c.CONFIG_MYSQL_URL));

/**
 * Create neo4j connection
 */
def.factory.graph = () => graph(get(c.CONFIG_NEO4J_URL));

/**
 * Create mongodb connection
 */
def.factory.mongo = async () => await mongo(
  get(c.CONFIG_MONGODB_URL),
  get(c.CONFIG_MONGODB_OPTIONS),
  get(c.CONFIG_DEBUG));

/**
 * Get sentry client in async way
 */
def.factory.sentryAsync = async container => new Promise((resolve, reject) => {
  const sentry = container.get('sentry');
  if (sentry) {
    resolve(sentry);
  } else {
    reject();
  }
});

export default new Container(def);
