import express from 'express';
import bodyParser from 'body-parser';
import compression from 'compression';
import cors from 'cors';
import helmet from 'helmet';
import jsend from 'jsend';
import lusca from 'lusca';
import statusMonitor from 'express-status-monitor';
import responseTime from 'response-time';
import debug from 'debug';
import { get } from './config';
import c from './config/constants';

import core from './modules/core';
import contrib from './modules/contrib';

const app = express();
const env = get(c.CONFIG_ENV);
const envLog = debug(env);

// catch global exception
// should be placed at the very begining
// see: https://docs.sentry.io/clients/node/integrations/connect/
core.container.get('sentry', (e, sentry) => (sentry ? sentry.install() : envLog(e)));

app.use(statusMonitor());
app.use(responseTime());
app.use(jsend.middleware);
app.use(cors());
app.use(helmet());
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// prevent clickjacking and cross site scripting
// see: https://github.com/krakenjs/lusca
app.use(lusca.xframe('SAMEORIGIN'));
app.use(lusca.xssProtection(true));

// set default express behavior
// disable x-powered-by signature
// and enable case-sensitive routing
app.set('env', env);
app.set('x-powered-by', false);
app.set('case sensitive routing', true);

// configure modules
// all modules that being mounted should be registered here
core.configure(app);
contrib.configure(app);

// initialize modules
// all middleware should be registered here
app.emit(c.MODULE_ON_INIT);

// sentry request handler
// should be the first item before registering any routes
// see: https://docs.sentry.io/clients/node/integrations/express/
core.container.get('sentry', (e, sentry) => (sentry ? app.use(sentry.requestHandler()) : envLog(e)));

app.emit(c.MODULE_ON_PRE_EXPOSE);
app.emit(c.MODULE_ON_EXPOSE);
app.emit(c.MODULE_ON_POST_EXPOSE);
app.emit(c.MODULE_ON_LOAD);

// sentry error handler
// should be coming first before any other error handler
// see: https://docs.sentry.io/clients/node/integrations/express/
core.container.get('sentry', (e, sentry) => (sentry ? app.use(sentry.errorHandler()) : envLog(e)));

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  // eslint-disable-next-line no-console
  console.error(err.stack);

  const errorId = res.sentry || 0;
  res.status(500).jsend.error(`An error has occured. Error ID: ${errorId}.`);
});

// eslint-disable-next-line no-unused-vars
app.use((req, res, next) => {
  res.status(404).jsend.fail('Not Found');
});

export default app;
