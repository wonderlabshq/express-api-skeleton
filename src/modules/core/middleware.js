import _ from 'lodash';
import logger from 'morgan';
import timeout from 'connect-timeout';
import { get } from '../../config';
import c from '../../config/constants';
import { isArray } from '../../lib/utils';

/**
 * Request logger middleware
 * @param {Array} env
 * @return {function}
 */
export function requestLoggerMiddleware(env) {
  if (env === undefined) {
    env = [c.ENV_DEVELOPMENT, c.ENV_STAGING, c.ENV_PRODUCTION];
  }

  if (!isArray(env)) {
    env = [env];
  }

  if (_.includes(env, get(c.CONFIG_ENV))) {
    return logger(get(c.CONFIG_LOGGER_FORMAT));
  }

  return (req, res, next) => next();
}

/**
 * Timeout middleware
 * @param {number} ms
 * @return {function}
 */
export function timeoutMiddleware(ms) {
  return timeout(ms);
}
