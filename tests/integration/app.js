/* eslint-disable */
import request from 'supertest';
import app from '../../src/app';

app.get('/500', () => {
  throw new Error();
});

describe('GET /404', () => {
  test('should return 404 Not Found', (done) => {
    request(app)
      .get('/404')
      .expect(404, done);
  });
});

describe('GET /500', () => {
  test('should return 500 Internal Server Error', (done) => {
    request(app)
      .get('/500')
      .expect(500, done);
  });
});
