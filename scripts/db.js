const fs = require('fs');
const path = require('path');
const knex = require('knex');
const chalk = require('chalk');
const task = require('./task');

const commands = ['version', 'migrate', 'migrate:rollback', 'migration', 'seed'];
const command = process.argv[2];
const envFiles = process.env.CONFIG_FILES || '';
const configObj = ['./config/public.json', './config/local.json']
  .concat(envFiles.split(','))
  .filter(f => f.endsWith('.json'))
  .filter(f => fs.existsSync(f))
  .map(p => fs.readFileSync(p, 'utf8'))
  .map(s => JSON.parse(s))
  .reduce((acc, val) => Object.assign(acc, val), {});

if (!configObj.mysqlUrl) {
  console.log(chalk.bgRed('MySQL URL is not set in config.'));
  console.log(chalk.bgRed('Database operation aborted!'));
  process.exit();
}

const config = {
  client: configObj.mysqlUrl.split(':')[0],
  connection: configObj.mysqlUrl,
  migrations: {
    tableName: 'migrations',
    directory: path.normalize(`${__dirname}/../migrations`),
  },
};

const version = new Date().toISOString().substr(0, 16).replace(/\D/g, '');
const template = `module.exports.up = async (db) => {\n  \n};\n
module.exports.down = async (db) => {\n  \n};\n
module.exports.configuration = { transaction: true };\n`;

module.exports = task('db', async () => {
  let db;

  if (!commands.includes(command)) {
    throw new Error(`Unknown command: ${command}`);
  }

  try {
    switch (command) {
      case 'version':
        db = knex(config);
        await db.migrate.currentVersion(config).then(console.log);
        break;
      case 'migration':
        fs.writeFileSync(`migrations/${version}_${process.argv[3] || 'new'}.js`, template, 'utf8');
        break;
      case 'migrate:rollback':
        db = knex(config);
        await db.migrate.rollback(config);
        break;
      case 'seed':
        console.error('Not yet implemented.');
        break;
      default:
        db = knex(config);
        await db.migrate.latest(config);
    }
  } finally {
    if (db) {
      await db.destroy();
    }
  }
});
