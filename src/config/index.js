import convict from 'convict';
import fs from 'fs';
import path from 'path';
import definitions from './definitions';

export const config = convict(definitions);

export function get(name) {
  return config.get(name);
}

export function getConfigFiles() {
  const publicConfig = path.normalize(`${__dirname}/../../config/public.json`);
  const localConfig = path.normalize(`${__dirname}/../../config/local.json`);
  const envFiles = process.env.CONFIG_FILES || '';
  return [publicConfig, localConfig]
    .concat(envFiles.split(','))
    .filter(f => f.endsWith('.json'))
    .filter(f => fs.existsSync(f));
}
